import React from 'react';
import { Link } from 'react-router-dom';

const PageNotFound = () => {
  return (
    <p>
      Error 404: Page Not found. Go to <Link to="/">Home</Link>
    </p>

  );
};

export default PageNotFound;