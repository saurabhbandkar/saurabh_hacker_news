import React, { Component } from 'react';
import 'react-dropdown/style.css';
import Dropdown from 'react-dropdown';
import { FaRegEye, FaRegEyeSlash } from 'react-icons/fa';
import axios from "axios";
import Error from './Error';

const { authSchema } = require("../utils/validate");
const options = [
    '1-18', '18-24', '25-32', '>32'
];
const defaultOption = options[0];

const defaultState = {
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
    nameError: '',
    emailError: '',
    passwordError: '',
    confirmPasswordError: '',
    showPassword: false,
    showConfirmPassword: false,
    isLoading: false,
    isLoaded: true,
}


class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: defaultState, isSignupAPIResponseOK: true
        };
    };

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value,
            nameError: "",
            emailError: "",
            passwordError: "",
            confirmPasswordError: ""
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        authSchema.validateAsync({
            user_name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            repeat_password: this.state.confirmPassword,
        })
            .then(() => {
                let signupData = {
                    user_name: this.state.name,
                    email: this.state.email,
                    password: this.state.password,
                };
                this.handleSignupData(signupData);
            })
            .catch((error) => {
                let errorMessages = {};
                if (error.message === '"email" is required') {
                    errorMessages.emailError = 'Email is required'
                } else if (error.message === '"email" must be a valid email') {
                    errorMessages.emailError = "Please choose a valid email"
                }
                if (error.message === '"user_name" is required') {
                    errorMessages.nameError = 'User name is required'
                } else if (error.message.includes('"user_name" with value')) {
                    errorMessages.nameError = 'Please provide a valid user name'
                }
                if (error.message === '"password" is required') {
                    errorMessages.passwordError = 'Password is required'
                } else if (error.message.includes('"password" with value')) {
                    errorMessages.passwordError = 'Password should be 1 capital, 1 small, 1 numeric, 1 special, 6 letter'
                }
                if (error.message === '"repeat_password" is required') {
                    errorMessages.confirmPasswordError = 'Confirm password is required'
                } else if (error.message.includes('"repeat_password" must be [ref:password]')) {
                    errorMessages.confirmPasswordError = 'Passwords do not match'
                };
                this.setState(errorMessages);
            });

    };

    handleSignupData = signupData => {
        this.setState({
            isLoading: true
        })
        axios.post(
            'https://redbus-apis.herokuapp.com/register',
            signupData
        )
            .then((response) => {
                let responseData = response.data;
                this.props.history.push('/login');
                this.setState({
                    isSignupAPIResponseOK: true,
                    isLoaded: true
                })
            })
            .catch((err) => {
                if (err.response) {
                    this.setState({
                        isSignupAPIResponseOK: err.response.status === 404 ? false : true,
                        isLoaded: true
                    });
                };
            })
    };
    handleShowPassword = () => {
        this.setState((prevState) => {
            return {
                showPassword: !prevState.showPassword,
            }
        });
    };

    handleShowConfirmPassword = () => {
        this.setState((prevState) => {
            return {
                showConfirmPassword: !prevState.showConfirmPassword,
            }
        });
    };

    render() {
        return (
            <>{!this.state.isSignupAPIResponseOK && <Error />}
                {this.state.isSignupAPIResponseOK &&
                    <div className="signupComponent">
                        <div className="Signup">
                            <div className="Signup-content pt-1">
                                <p className="mb-4">
                                </p>
                                <div className="HeadingImgWrapper">
                                    <h1 className="Heading">Sign up</h1>
                                </div>
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group mb-3">
                                        <label className="form-label" htmlFor="name">
                                            Name
                                        </label>
                                        <input
                                            className="form-control"
                                            type="text"
                                            id="name"
                                            name="name"
                                            placeholder="First Name"
                                            value={this.state.name}
                                            onChange={this.handleChange}
                                        />
                                        <small className="text-danger">{this.state.nameError}</small>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label className="form-label" htmlFor="email">
                                            E-mail
                                        </label>
                                        <input
                                            className="form-control"
                                            type="text"
                                            id="email"
                                            name="email"
                                            placeholder="example@domain.com"
                                            value={this.state.email}
                                            onChange={this.handleChange}
                                        />
                                        <small className="text-danger">{this.state.emailError}</small>
                                    </div>

                                    <div className="form-group mb-3">
                                        <label className="form-label" htmlFor="age">
                                            Please Select Your Age Group
                                        </label>
                                        <Dropdown options={options} onChange={this._onSelect} value={defaultOption} placeholder="Select your age group" />
                                    </div>

                                    <div className="form-group mb-3">
                                        <label className="form-label" htmlFor="password">
                                            Password
                                        </label>
                                        <div className="input-group">
                                            <input
                                                className="form-control"
                                                type={this.state.showPassword ? "text" : "password"}
                                                id="password"
                                                name="password"
                                                autoComplete="password"
                                                placeholder="password"
                                                value={this.state.password}
                                                onChange={this.handleChange}
                                            />
                                            <div className="input-group-append">
                                                <button className="btn btn-outline-secondary" type="button" onClick={this.handleShowPassword}>
                                                    {this.state.showPassword ? <FaRegEyeSlash /> : <FaRegEye />}
                                                </button>
                                            </div>
                                        </div>
                                        <small className="text-danger">{this.state.passwordError}</small>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label className="form-label" htmlFor="confirmPassword">
                                            Confirm Password
                                        </label>
                                        <div className="input-group">
                                            <input
                                                className="form-control"
                                                type={this.state.showConfirmPassword ? "text" : "password"}
                                                autoComplete="confirmPassword"
                                                id="confirmPassword"
                                                name="confirmPassword"
                                                placeholder="confirm password"
                                                value={this.state.confirmPassword}
                                                onChange={this.handleChange}
                                            />
                                            <div className="input-group-append">
                                                <button className="btn btn-outline-secondary" type="button" onClick={this.handleShowConfirmPassword}>
                                                    {this.state.showConfirmPassword ? <FaRegEyeSlash /> : <FaRegEye />}
                                                </button>
                                            </div>
                                        </div>
                                        <small className="text-danger">
                                            {this.state.confirmPasswordError}
                                        </small>
                                    </div>
                                    <div className="ButtonSpinnerWrapper">
                                        <button type="submit" className="btn btn-primary mr-2">
                                            Sign up
                                        </button>
                                        {
                                            this.state.isLoading &&
                                            <div className="spinner-border SpinnerStyle" role="status"></div>
                                        }
                                        <button type="button" onClick={() => { this.props.history.push('/login') }} className="btn btn-primary mr-2">
                                            Login
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                }
            </>
        );
    }
}

export default Signup;
