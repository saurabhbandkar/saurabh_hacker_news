import React from 'react';
import { Link } from "react-router-dom";
import moment from "moment";

export default function ShowStories({ stories, searchKeyword }) {
    if (searchKeyword === "") {
        const story = stories.map(story => {
            const { id, by, title, kids, url } = story.data;
            return (
                <div className="story">
                    <div className="story-title">
                        <Link to={{ pathname: url }} target={"_blank"} >
                            {title}
                        </Link>
                    </div>
                    <div className="story-info">
                        <span>
                            by{' '}
                            <Link to={{ pathname: `https://news.ycombinator.com/user?id=${by}` }} target={"_blank"} >
                                {by}
                            </Link>
                        </span>
                        |<span>
                            {
                                moment().format()
                            }
                        </span>|
                        <span>
                            <Link to={{ pathname: `https://news.ycombinator.com/item?id=${id}` }} target={"_blank"} >
                                {`${kids && kids.length > 0 ? kids.length : 0} comments`}
                            </Link>
                        </span>
                    </div>
                </div >
            );
        });
        return <div>{story}</div>
    } else {
        const story = stories.map(story => {
            const { id, by, title, kids, url } = story.data;
            if (title.toLowerCase().includes(searchKeyword.toLowerCase())) {
                return (
                    <div className="story">
                        <div className="story-title">
                            <Link to={{ pathname: url }} target={"_blank"} >
                                {title}
                            </Link>
                        </div>
                        <div className="story-info">
                            <span>
                                by{' '}
                                <Link to={{ pathname: `https://news.ycombinator.com/user?id=${by}` }} target={"_blank"} >
                                    {by}
                                </Link>
                            </span>
                            |<span>
                                {
                                    moment().format()
                                }
                            </span>|
                            <span>
                                <Link to={{ pathname: `https://news.ycombinator.com/item?id=${id}` }} target={"_blank"} >
                                    {`${kids && kids.length > 0 ? kids.length : 0} comments`}
                                </Link>
                            </span>
                        </div>
                    </div >
                );
            }
            else {
                return null;
            };
        });
        const nullValues = story.filter((element) => {
            return element === null;
        })
        if (nullValues.length !== 30) {
            return <div>{story}</div>
        } else {
            return (
                <div>
                    <h1>
                        {"No search Results Found, Use fewer keywords!!!"}
                    </h1>
                </div>
            )
        }

    };
};


