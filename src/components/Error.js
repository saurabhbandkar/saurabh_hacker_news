import React from 'react';

export default function Error() {
  return (
    <p>
      Error 500: Error Loading News Search. Kindly check news API URL.
      Please try again after sometime.
    </p>
  );
};
