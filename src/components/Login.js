import React, { Component } from 'react';
import { FaRegEye, FaRegEyeSlash } from 'react-icons/fa';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import Error from './Error';

const { loginAuthSchema } = require("../utils/validate");

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            emailError: '',
            passwordError: '',
            showPassword: false,
            isLoading: false,
            redirect: null,
            isAPIResponseOK: true
        };
    }

    handleChange = event => {
        this.setState((state) => {
            return {
                [event.target.name]: event.target.value,
                emailError: "",
                passwordError: "",
            }
        });
    };

    handleLoginData = (loginData) => {
        localStorage.clear();

        this.setState({
            isLoading: true
        }, () => {
            axios.post('https://redbus-apis.herokuapp.com/login', loginData)
                .then((response) => {
                    let responseData = response.data
                    localStorage.setItem("accessToken", responseData.accessToken);
                    localStorage.setItem("refreshToken", responseData.refreshToken);
                    this.setState({
                        isLoading: false,
                        isAPIResponseOK: true
                    }, () => {
                        this.setState({
                            redirect: '/new',
                            isAPIResponseOK: true
                        })
                    });
                })
                .catch((err) => {
                    this.setState({
                        isLoading: false,
                        passwordError: err.response && err.response.data && (err.response.data.msg === "Password Incorrect") ? err.response.data.msg : "",
                        emailError: err.response && err.response.data && (err.response.data.msg === "User doesn't exist") ? err.response.data.msg : "",
                        isAPIResponseOK: err.response.status === 404 ? false : true
                    });
                });
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        loginAuthSchema.validateAsync({
            email: this.state.email,
            password: this.state.password
        })
            .then(() => {
                let loginData = {
                    email: this.state.email.trim(),
                    password: this.state.password
                }
                this.handleLoginData(loginData);
            })
            .catch((error) => {
                let errorMessages = {};

                if (error.message === '"email" is required') {

                    errorMessages.emailError = 'Email is required';

                } else if (error.message === '"email" must be a valid email') {

                    errorMessages.emailError = 'Please choose a valid email';

                }
                if (error.message === '"password" is required') {

                    errorMessages.passwordError = 'Password is required';

                }
                this.setState(errorMessages);
            });
    };

    handleShowPassword = () => {
        this.setState((prevState) => {
            return {
                showPassword: !prevState.showPassword,
            }
        });
    };

    render() {

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        } else {
            return (
                <>{!this.state.isAPIResponseOK && <Error />}
                    {this.state.isAPIResponseOK &&
                        <div className="LoginFormWrapper">
                            <form onSubmit={this.handleSubmit} className="Loginform">
                                <div className="HeadingImgWrapper">
                                    <h1 className="Heading">Sign in</h1>
                                </div>

                                <div className="form-group mb-3">
                                    <label>Email</label>
                                    <input className="form-control"
                                        type="text"
                                        name="email"
                                        value={this.state.email}
                                        onChange={this.handleChange}
                                        placeholder="example@domain.com" />
                                    <small className="form-text text-danger">{this.state.emailError}</small>
                                </div>

                                <div className="form-group mb-3">
                                    <label>Password</label>
                                    <div className="input-group">
                                        <input className="form-control"
                                            type={this.state.showPassword ? "text" : "password"}
                                            value={this.state.password}
                                            name="password"
                                            onChange={this.handleChange}
                                            placeholder="password" />
                                        <div className="input-group-append">
                                            <button className="btn btn-outline-secondary" type="button" onClick={this.handleShowPassword}>
                                                {this.state.showPassword ? <FaRegEyeSlash /> : <FaRegEye />}
                                            </button>
                                        </div>
                                    </div>
                                    <small className="form-text text-danger">{this.state.passwordError}</small>
                                </div>

                                <div className="ButtonSpinnerWrapper">
                                    <button className="btn btn-primary mr-2" type="submit">Login</button>
                                    {
                                        this.state.isLoading &&
                                        <div className="spinner-border SpinnerStyle" role="status"></div>
                                    }

                                    <button type="button" onClick={() => { this.props.history.push('/') }} className="btn btn-primary mr-2">
                                        Signup
                                    </button>
                                </div>
                            </form>
                        </div>
                    }
                </>
            )
        };
    };
};

export default Login;
