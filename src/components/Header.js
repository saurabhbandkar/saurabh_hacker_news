import React from 'react';
import { NavLink } from 'react-router-dom';

export default class Header extends React.Component {
  constructor(props) {
    super(props)
  }
  
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-#F1CB38 bg-dark">
          <div className="container">
            <div className="row-sm">
              <h1> Hacker News Search</h1>
            </div>
            <div className="row-sm flexdisplay">
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto text-center">

                  <li className="nav-item">
                    <NavLink
                      onClick={() => this.props.getStories('topstories')}
                      to="/top"
                      activeClassName="active"
                    >
                      Top Stories
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink
                      to="/new"
                      activeClassName="active"
                      onClick={() => this.props.getStories('newstories')}
                    >
                      Latest Stories
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink
                      to="/best"
                      activeClassName="active"
                      onClick={() => this.props.getStories('beststories')}
                    >
                      Best Stories
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" onChange={(event) => this.props.getSearch(event.target.value)} />
                  </li>
                </ul>
              </div>

            </div>
          </div>
        </nav>
      </div >
    )
  };
};