import React, { Component } from 'react'
import {
    BrowserRouter,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
import Header from './components/Header';
import PageNotFound from './components/PageNotFound';
import ShowStories from './components/ShowStories';
import Loader from './components/Loader';
import Error from './components/Error';
import Signup from './components/Signup';
import Login from './components/Login';
import axiosInstance from './helpers/axiosInstance'


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stories: [],
            isLoaded: true,
            searchKeyword: "",
            isAPIResponseOK: true,
            isRegistered: true,
        };
        localStorage.setItem("userVerified", false)
    };


    componentDidMount() {
        localStorage.clear();
        axiosInstance
            .get("/newstories.json?print=pretty")
            .then(storyIds => {
                return Promise.all(
                    storyIds.data.slice(0, 30).map(storyId => this.getStory(storyId))
                );
            })
            .then(topStories => {
                this.setState({
                    stories: topStories,
                    isLoaded: true,
                    isAPIResponseOK: true
                });
            })
            .catch((error) => {
                console.log("Error loading the news API URL");
                this.setState({
                    isAPIResponseOK: false,
                    isLoaded: true
                });
            });
    }
    getStories = (type) => {
        this.setState({
            isLoaded: false,
        }, (state) => {
            axiosInstance
                .get(`/${type}.json`)
                .then(storyIds => {
                    return Promise.all(
                        storyIds.data.slice(0, 30).map(storyId => this.getStory(storyId))
                    );
                })
                .then(selectTopic => {
                    this.setState({
                        stories: selectTopic,
                        isLoaded: true,
                        isAPIResponseOK: true
                    });
                })
                .catch((error) => {
                    console.log("Error loading the news API URL");
                    this.setState({
                        isAPIResponseOK: false,
                        isLoaded: true
                    });
                });
        }
        );



    };

    getStory = (id) => {
        return axiosInstance
            .get(
                `/item/${id}.json`
            )
            .then((story) => {
                return story;
            })
            .catch((error) => {
                console.error('Error while getting a story.');
                this.setState({
                    isAPIResponseOK: false
                })
            });
    };
    getSearch = (key) => {
        this.setState({
            searchKeyword: key
        })
    };


    render() {

        return (
            <div>
                <BrowserRouter>
                    <div className="container">
                        <Header getStories={this.getStories} getSearch={this.getSearch} />

                        {!this.state.isLoaded && <Loader />}

                        {
                            this.state.isAPIResponseOK ?
                                <Switch>
                                    <Route path="/" render={() => <Redirect to="/signup" />} exact={true} component={Signup} />
                                    <Route path="/login" component={Login} />
                                    <Route
                                        path="/:type"
                                        render={({ match }) => {
                                            const { type } = match.params;
                                            if (!['top', 'new', 'best'].includes(type)) {
                                                return <PageNotFound />;
                                            }
                                            if (localStorage.getItem("accessToken")) {
                                                return <ShowStories stories={this.state.stories} searchKeyword={this.state.searchKeyword} />;
                                            } else {
                                                return <Login />;
                                            }

                                        }}
                                    />
                                </Switch>
                                :
                                <Error />
                        }
                    </div>
                </BrowserRouter>
            </div>
        )
    }
}