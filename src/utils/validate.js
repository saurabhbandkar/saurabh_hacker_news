const Joi = require('joi');

const authSchema = Joi.object({
    user_name: Joi.string()
        .pattern(new RegExp("^[A-Za-z]+$"))
        .required(),

    email: Joi.string()
        .email({ tlds: { allow: false } })
        .lowercase()
        .required(),

    password: Joi.string()
        .pattern(new RegExp("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$"))
        .required(),

    repeat_password: Joi.any().valid(Joi.ref('password')).required()
});

const loginAuthSchema = Joi.object({
    email: Joi.string()
        .email({ tlds: { allow: false } })
        .lowercase()
        .required(),

    password: Joi.string()
        .required(),
});

module.exports = {
    authSchema,
    loginAuthSchema
};